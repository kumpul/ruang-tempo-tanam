import * as tanam from 'tanam';

tanam.initializeApp(process.env.GCLOUD_PROJECT === 'ruangtempo'
    ? {
        users: {
            "faye@kumpul.id": "superAdmin",
            "dennis@kumpul.id": "superAdmin",
            "deta@oddbit.id": "superAdmin",
            "ananda@oddbit.id": "superAdmin",
            "bella@kumpul.id": "admin",
            "andreas@kumpul.id": "admin",
            "geovania@kumpul.id": "admin",
            "irfan@kumpul.id": "admin",
            "richard@kumpul.id": "admin",
            "tommy@ruangdantempo.id": "admin"
        },
        firebaseApp: {
            apiKey: "AIzaSyAwCAXPblXlEKvuZls4mCOoXxLXoksKMUE",
            authDomain: "ruangtempo.firebaseapp.com",
            databaseURL: "https://ruangtempo.firebaseio.com",
            projectId: "ruangtempo",
            storageBucket: "ruangtempo.appspot.com",
            messagingSenderId: "905008556699"
        },
    }
    : {
    users: {
        "dennis@oddbit.id": "superAdmin",
        "deta@oddbit.id": "superAdmin",
        "ananda@oddbit.id": "superAdmin",
    },
    firebaseApp: {
        apiKey: "AIzaSyDklPr1AZMlUNnZF_JBiklHYRf4ZT1us8A",
        authDomain: "ruangtempo-dev.firebaseapp.com",
        databaseURL: "https://ruangtempo-dev.firebaseio.com",
        projectId: "ruangtempo-dev",
        storageBucket: "ruangtempo-dev.appspot.com",
        messagingSenderId: "681682867439"
    },
});

export * from 'tanam';
